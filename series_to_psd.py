import numpy as np

def series_to_psd (series, dt) :

  """
  Take a timeseries and compute 
  its PSD
  """
  freq = np.fft.rfftfreq (series.size, d=dt)
  tf = np.fft.rfft (series) / (series.size / 2.)
  T = series.size * dt
  PSD = np.abs (tf) * np.abs (tf) * T / 2.

  return freq, PSD
