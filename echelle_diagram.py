import numpy as np
import matplotlib
matplotlib.use ('TkAgg')
import matplotlib.pyplot as plt
import pandas as pd

def build_diagram (freq, PSD, dnu, plot=True, twice=False,
                   smooth=10, cmap='inferno') :

  if smooth != 1 :
    PSD = pd.Series(data=PSD).rolling (window=smooth, min_periods=1, 
                             center=True).mean().to_numpy()

  if twice==True :
    dnu = 2.*dnu

  res = freq[2]-freq[1]
  start = int (freq[0]*1.e6 / dnu + 1.) * dnu
  stop = int (freq[-1]*1.e6 / dnu) * dnu
 
  ind, = np.where ((freq > start*1e-6) & (freq < stop*1e-6))
  freq = freq[ind]
  PSD = PSD[ind]

  n_slice = int (round (freq.size*res*1.0e6 / dnu))
  len_slice = int (round ((freq[-1]-freq[0]) / (n_slice*res)))

  if (n_slice*len_slice > PSD.size) :
    len_slice -= 1

  ed = PSD[:len_slice*n_slice]
  ed = np.reshape (ed, (n_slice, len_slice))

  freq_ed = freq[:len_slice*n_slice]
  freq_ed = np.reshape (freq_ed, (n_slice, len_slice))
  x_freq = freq_ed[0,:] - freq_ed[0,0]
  y_freq = freq_ed[:,0]

  if plot==True :
    fig = plt.figure ()
    ax = fig.add_subplot (111)
    ax.pcolormesh (x_freq*1.e6, y_freq*1.e6, ed, cmap=cmap)
    ax.set_xlabel (r'Frequency mod. ' + str('%.1f' % dnu) + r' $\mu$Hz')
    ax.set_ylabel (r'Frequency ($\mu$Hz)')
    plt.show ()

  return ed
